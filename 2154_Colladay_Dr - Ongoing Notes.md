**2154 Collady Point Meeting 09/07/21 14:00 CST**

*7 Points from John & Jennifer:*

1. Option 2( preferred)

2. Less tile (options needed)

3. Extend dining room window

4. No transparent glass for garage[perhaps frosted]

5. Front facade[North] to have corner windows found in other parts(operable windows, need to split up window)

6. Trusses?[Wood]

7. Solar panels? [Need to explore but limiting on South facade]

<<<<<<< HEAD
=======

> > > > > > > 2e088cdedb6253a6b99260d329a9d02b5ad1239a
> > > > > > > *Other notes:*

-Radiant heat on second floor? [Perhaps just in master bedroom]

-Hardwood floors on second floor

Jennifer: "Engineered floor"

-Exterior radiant heating?[Non: "Did it 20 years ago.....not common"]

-Garage to be heat but not radiant heating.

-Trees [John: " Keep a couple"]

1 cottonwood, 1 oak tree

-Boathouse[flat roof with stairs]

-Breaking ground? Fall? Spring? [Bob: "About Dane County zoning? It was horrible."]

-Most likely spring[March 2022]

-3-4 months wait for window fixtures, Bob: "better to have all the materials ready to go."

-Bob:"Lumber prices are falling."

-Jennifer: "Boathouse, dump costs there!"

- Questions
  - For client
    - internal gutter?.. more prone to leaking
    - <!--stackedit_data:
      eyJoaXN0b3J5IjpbMTk1OTU1NzQ1NCwxMDE5NTQwNDczXX0=
      -->

**2154 Collady Point Meeting 11/09/21 16:00 CST**

*Yes/No points from John/Jennifer:*

No: Smaller windows

Yes: Clerestory in living room

No: Portal frame in living room(1st floor) where veneer wraps around

Yes: Drywall ceiling on 1st floor

No: Tie rods but Ryan explains the benefits of them, up for debate

Yes: Exposed trusses on 2nd floor

Yes: Continue siding in 2nd floor living room

No: Brick veneer

Yes: **[1. Jen's pillar]** Field stone or concrete 'look; *Bob suggests 'cut stone'; to look into cost comparisons between stone/concrete finishes*

No: Boathouse

No: Radiant heating on 2nd floor

Yes: 9'-8" ceiling

Yes: Outer gutters

No: Roof eaves

Yes: Batten board

Yes: ****[2. Jen's pillar]**: Cost comparison of metal seaming and batten board

Yes: **[3. Jen's pillar]**:**Cost comparison of different trusses

No: Spiral stairs, Ryan suggesting a ladder approach, Jon into the idea

Yes: Jen wants to enlarge pool, 7'x13'

Yes: Jon asks for 'back of house generator'; Bob says it's possible

Yes: High end vinyl wood flooring product; Cali Floors

Yes: Wrapping pos in 2nd floor living room and master bedroom with siding[Jen: less money to spend on art]

Yes: Jon wants snow bars on roof as advised by Ryan



**2154 Collady Point Meeting 11/30/21 13:00 CST**

-[JON] likes stone veneer.

-[JEN] prefers concrete(concrete was decided on).

-[JEN] LVT flooring preferred due to savings which allows for a partial house generator.

-[JEN]Standing seam for vertical surface.

-[JEN] Generator on outside.

-No spiral stairs.

-[BOB] March 15th for 75%?

-[BOB] Combined set for a permit on February 15th.

-[RYAN] 2 months to produce 75% set.

-[RYAN] Further decisions needed to be made regarding ofmaterials, "at 50% in terms of material selection."

-Trees down in winter and drill well (mid to late January).

-[JON] Include boathouse part of permitting process?(plans to build in future).

-[RYAN] Apply for a permit with boathouse.

-[BOB] Take down cottonwood tree located where planned boathouse will be?

-[JON] Leave the cottonwood tree for now.

-[BOB] Will we have an interior decorator?

-[JEN] Will try without interior decorator to save money.

-[RYAN]  "With Pinterest board, we can make recommendations." 

-[RYAN] Tim for casework.

-[JEN] ''Looking for something pretty simple"(mentions subway tile in current kitchen).

-[BOB] To recommend fixtures.

-[RYAN] "We'll send recommendations(regarding fixtures)."

-[BOB] Urges sooner to order plumbing fixtures.

-[JON] Wants to apply the same urgency to order fixtures for lighting.

-[BOB] "Not a complicated lighting project."

-[JEN] Envisions special lighting fixtures over the kitchen island, dining table, entryway.

-[BOB]"Motorized shades?"

-Jen and Jon did not specify if they wish to have that but want a security system.

-[JEN]"I rather go crazy on the siding."

-[JEN]"I'll post to Pinterest board for fixtures."

-Jon to pick out appliances.

-[JON]"Icemaker in scullery or work space?"

-[BOB]"Icemaker generates a lot of heat."(depends on HVAC requirements)

-[BOB]"How elaborate will the closets be? Closet companies charge four times more than everyone else."

-Bob encourages Jen and Jon to get idea what the closet should look like.

-Jon and Jen to focus on selecting bathroom fixtures, icemaker.

-Jen will 'throw stuff on the {Pinterest} board.'

-[BOB] Wants to see a foundation plan with 3D schematic.

-Dane County submissions first then foundation plan.