# Wind load on the steel structure.
We have managed to keep the steel structure as it is (without connecting it to the concrete wall) by reducing its importance as a component of the shear resisting system.

![South west module](./south_east_module_shear_resistance_scheme.png)

The steel structure is quite flexible, so it's hard to make it work as a shear wall without taking non-negligible displacements (a bit more than an inch) which are excessive. To solve this, we rely on the stiffness of the east and west concrete walls, to compensate for the small reaction (and small displacement hence) of the steel structure. This way, there is no variation of the dimensions of the steel structure. The only changes we need to introduce are in the reinforcement of the steel structure footings and the orientation of the short columns on top of the concrete walls.


