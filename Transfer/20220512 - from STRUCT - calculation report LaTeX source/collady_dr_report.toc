\contentsline {section}{\numberline {1}Executive summary}{3}{section.1}%
\contentsline {section}{\numberline {2}Codes}{3}{section.2}%
\contentsline {section}{\numberline {3}Loading criteria}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Dead load (DL)}{5}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Live loads (LL)}{6}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Main building}{6}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Tornado shelter}{6}{subsubsection.3.2.2}%
\contentsline {subsection}{\numberline {3.3}Snow load (SL)}{6}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Wind design criteria}{11}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Main building}{11}{subsubsection.3.4.1}%
\contentsline {paragraph}{Directionality factor}{11}{section*.8}%
\contentsline {paragraph}{Ground elevation factor}{11}{section*.9}%
\contentsline {paragraph}{Gust-effect factor}{11}{section*.10}%
\contentsline {paragraph}{Velocity pressure exposure coefficient $K_z$}{11}{section*.11}%
\contentsline {paragraph}{Velocity pressure}{11}{section*.12}%
\contentsline {paragraph}{Internal pressure coefficient $GC_{pi}$}{13}{section*.14}%
\contentsline {subsection}{\numberline {3.5}Tornado shelter}{16}{subsection.3.5}%
\contentsline {section}{\numberline {4}Materials}{16}{section.4}%
\contentsline {subsection}{\numberline {4.1}Reinforced concrete}{16}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Structural steel}{17}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Wood structural panels}{17}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Laminated veneer lumber (LVL)}{17}{subsubsection.4.3.1}%
\contentsline {section}{\numberline {5}Load combinations}{18}{section.5}%
\contentsline {section}{\numberline {6}Serviceability}{18}{section.6}%
\contentsline {section}{\numberline {7}Design technique}{19}{section.7}%
\contentsline {subsection}{\numberline {7.1}Design and analysis software}{19}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Horizontal distribution of wind forces to shear walls and steel structure}{20}{subsection.7.2}%
\contentsline {subsubsection}{\numberline {7.2.1}Introduction}{20}{subsubsection.7.2.1}%
\contentsline {subsubsection}{\numberline {7.2.2}Analysis of the shear distribution}{20}{subsubsection.7.2.2}%
\contentsline {subsection}{\numberline {7.3}Steel structure}{26}{subsection.7.3}%
\contentsline {subsubsection}{\numberline {7.3.1}Geometry idealization and model}{26}{subsubsection.7.3.1}%
\contentsline {subsubsection}{\numberline {7.3.2}Design for stability}{26}{subsubsection.7.3.2}%
\contentsline {paragraph}{Direct analysis method}{26}{section*.33}%
\contentsline {paragraph}{Calculation of required strenghts}{26}{section*.34}%
\contentsline {subparagraph}{Consideration of initial system imperfections}{26}{section*.35}%
\contentsline {subparagraph}{Adjustments to stiffness}{26}{section*.36}%
\contentsline {paragraph}{Calculation of available strengths}{28}{section*.37}%
\contentsline {subsubsection}{\numberline {7.3.3}Graphical output}{28}{subsubsection.7.3.3}%
\contentsline {section}{\numberline {8}Timber structure}{29}{section.8}%
\contentsline {subsection}{\numberline {8.1}Trusses}{29}{subsection.8.1}%
\contentsline {subsubsection}{\numberline {8.1.1}Introduction}{29}{subsubsection.8.1.1}%
\contentsline {subsubsection}{\numberline {8.1.2}Second floor trusses deflections}{29}{subsubsection.8.1.2}%
\contentsline {subsection}{\numberline {8.2}Joists}{29}{subsection.8.2}%
\contentsline {subsubsection}{\numberline {8.2.1}Roof rafters design}{29}{subsubsection.8.2.1}%
\contentsline {subsubsection}{\numberline {8.2.2}Scissor trusses}{32}{subsubsection.8.2.2}%
\contentsline {subsubsection}{\numberline {8.2.3}Roof joists and beams}{32}{subsubsection.8.2.3}%
\contentsline {subsubsection}{\numberline {8.2.4}Attic joists}{32}{subsubsection.8.2.4}%
\contentsline {subsubsection}{\numberline {8.2.5}Second floor corridors beams and joists}{36}{subsubsection.8.2.5}%
\contentsline {subsection}{\numberline {8.3}Headers}{40}{subsection.8.3}%
\contentsline {subsubsection}{\numberline {8.3.1}Second floor headers}{40}{subsubsection.8.3.1}%
\contentsline {subsubsection}{\numberline {8.3.2}First floor headers}{42}{subsubsection.8.3.2}%
\contentsline {subsubsection}{\numberline {8.3.3}Garage lintels}{42}{subsubsection.8.3.3}%
\contentsline {subsection}{\numberline {8.4}Built-up columns}{44}{subsection.8.4}%
\contentsline {subsubsection}{\numberline {8.4.1}Second floor columns}{44}{subsubsection.8.4.1}%
\contentsline {subsubsection}{\numberline {8.4.2}First floor columns}{44}{subsubsection.8.4.2}%
\contentsline {subsection}{\numberline {8.5}Second floor bearing walls}{44}{subsection.8.5}%
\contentsline {subsubsection}{\numberline {8.5.1}Loads on second floor walls}{44}{subsubsection.8.5.1}%
\contentsline {subsubsection}{\numberline {8.5.2}Load combinations}{44}{subsubsection.8.5.2}%
\contentsline {subsubsection}{\numberline {8.5.3}Stud material and arrangement}{48}{subsubsection.8.5.3}%
\contentsline {subsubsection}{\numberline {8.5.4}Stud design}{48}{subsubsection.8.5.4}%
\contentsline {subsection}{\numberline {8.6}Second floor shear walls}{48}{subsection.8.6}%
\contentsline {section}{\numberline {9}Steel structure, results of analysis}{48}{section.9}%
\contentsline {subsection}{\numberline {9.1}Load patterns}{50}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Check under normal stresses}{50}{subsection.9.2}%
\contentsline {subsection}{\numberline {9.3}Check under shear stresses}{50}{subsection.9.3}%
\contentsline {subsection}{\numberline {9.4}Check displacements for SLS}{50}{subsection.9.4}%
\contentsline {subsection}{\numberline {9.5}Foundation soil pressures for SLS}{50}{subsection.9.5}%
\contentsline {section}{\numberline {10}Tornado shelter, results of analysis}{69}{section.10}%
\contentsline {subsection}{\numberline {10.1}Shelter load patterns}{71}{subsection.10.1}%
\contentsline {subsection}{\numberline {10.2}Check under normal stresses}{71}{subsection.10.2}%
\contentsline {section}{\numberline {11}RC walls, results of analysis}{76}{section.11}%
\contentsline {subsection}{\numberline {11.1}Load patterns}{76}{subsection.11.1}%
\contentsline {subsection}{\numberline {11.2}Check under normal stresses}{76}{subsection.11.2}%
\contentsline {subsection}{\numberline {11.3}Check under shear stresses}{78}{subsection.11.3}%
\contentsline {section}{\numberline {A}\ignorespaces Hazards Report ASCE/SEI 7-22}{83}{appendix.A}%
\contentsline {section}{\numberline {B}APA wall bracing calculator results}{90}{appendix.B}%
\contentsline {subsection}{\numberline {B.1}\ignorespaces North-East module results}{91}{subsection.B.1}%
\contentsline {subsection}{\numberline {B.2}\ignorespaces North-West module results}{101}{subsection.B.2}%
\contentsline {subsection}{\numberline {B.3}\ignorespaces South-East module results}{111}{subsection.B.3}%
\contentsline {subsection}{\numberline {B.4}\ignorespaces South-West module results}{121}{subsection.B.4}%
\contentsline {section}{\numberline {C}\ignorespaces Shear walls required capacities}{133}{appendix.C}%
