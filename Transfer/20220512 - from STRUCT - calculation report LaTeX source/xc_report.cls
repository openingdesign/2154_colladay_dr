% This document class provides a simple memo for LaTeX users.
% It is based on article.cls and inherits most of the functionality
% that class.
% 
% Author: Rob Oakes, Copyright 2010.  Released under the LGPL, version 3.
% A copy of the LGPL can be found at http://www.gnu.org/licenses/lgpl.html

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{xc_report}[2010/07/31 - Simple Report Class, Including Logo]
\RequirePackage{palatino}
\RequirePackage{fancyhdr}
\RequirePackage{geometry}
\RequirePackage{lastpage}
\RequirePackage{svg}


\newcommand{\revision}{0.0}
\newcommand{\reportLogo}{\includegraphics[height=3.5mm]{xc_report_logo}}
%%\newcommand{\reportLogo}{\includesvg{xc_report_logo}}

% Load the Base Class
\LoadClassWithOptions{article}

%%Geometría de la página.
\geometry{a4paper,portrait,left=2cm,right=2cm,top=3.5cm, bottom=2cm}
\oddsidemargin=10mm
\evensidemargin=0mm
\textwidth=150mm
\textheight= 22cm 


\pagestyle{fancy}
\fancypagestyle{plain}{
\fancyhf{} %anula los valores de fancy por defecto 

\fancyfoot[LE]{pag. \thepage\ de \pageref{LastPage}}
\fancyfoot[CE]{\reportLogo}
\fancyfoot[RE]{\emph{rev. \revision}}

\fancyfoot[LO]{\emph{\date}}
\fancyfoot[CO]{\reportLogo}
\fancyfoot[RO]{pag. \thepage\ de \pageref{LastPage}}
\renewcommand{\headrulewidth}{0pt} %Dibuja una raya debajo de la cabecera
\renewcommand{\footrulewidth}{0pt}
}

\renewcommand{\headrulewidth}{0.1pt} %Dibuja una raya debajo de la cabecera
\renewcommand{\footrulewidth}{0pt}
%\marginparwidth=0mm

\fancyhf{} %anula los valores de fancy por defecto 

\fancyhead[LE]{\textsc{\title}}
%\fancyhead[RO]{\textsc{\leftmark}}
\fancyhead[RO]{\textsc{\rightmark}}

\fancyfoot[LE]{pag. \thepage\ de \pageref{LastPage}}
\fancyfoot[CE]{\reportLogo}
\fancyfoot[RE]{\emph{rev. \revision}}

\fancyfoot[LO]{\emph{\date}}
\fancyfoot[CO]{\reportLogo}
\fancyfoot[RO]{pag. \thepage\ de \pageref{LastPage}}



