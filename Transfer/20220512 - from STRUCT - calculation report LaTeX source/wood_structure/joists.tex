\subsection{Joists}
Joists are used to frame the following parts of the building:

\begin{itemize}[noitemsep]
  \item The roof.
  \item The attic floors.
  \item The corridors on the second floor.
\end{itemize}    

\subsubsection{Roof rafters design}
Roof rafters are spaced 24 inches on center, they span 3.254 m (10.67 feet) between the ridge and the supporting wall plates. The rafters are 2x8 dimensional lumber pieces made of southern pine structural grade\footnote{We have also checked an alternate design using TJI PRI-30 (9-1/2 in) performance rated I-joists and the results are equally satisfactory.}.\\

The load combinations used for strength design are:

\begin{center}
  \begin{small}
  \begin{tabular}{|l|l|}
    \hline
    LC1 & D+RL \\
    LC2 & D+S \\
    LC3 & 0.6*D+Wu \\
    LC4 & D+W \\
    \hline
  \end{tabular}
  \end{small}
  \end{center}

\noindent where:

\begin{description}[noitemsep]
\item{D:} Dead load.
\item{RL:} Roof live load: $0.24\ kN/m^2$ (5 psf).
\item{S:} Snow load.
\item{W:} Wind load.
\end{description}

Figure \ref{roof_load_distribution} presents the values of the loads on this members, except for the live load in the gable portion, which is $0.24\ kN/m^2$ (5 psf). The calculation results are presented in table \ref{tb_roof_rafters_structural_integrity}.\\

\begin{table}
\begin{center}
\begin{small}
  \begin{tabular}{lrrrrrr}
    \hline
    \multicolumn{7}{c}{Roof rafters structural integrity calculation results}\\
\hline
Load case & $CD$ & $M_{z}$ &   $CL$ &   $\nu$ &     $V_{y}$ & $\mu$ \\
          &      & $(kN \cdot m)$ &   &      &     $(kN)$    &  \\
\hline
 LC1 & 1.25 &  1.39 &    1 &        0.34 &  1.58 &      0.28 \\
 LC2 & 1.15 &  2.6  &    1 &        0.68 &  2.96 &      0.53 \\
 LC3 & 1.6  &  0.39 &    1 &        0.07 &  0.45 &      0.08 \\
 LC4 & 1.6  &  1.62 &    1 &        0.3  &  1.84 &      0.33 \\
\hline
\multicolumn{7}{l}{where:}\\
\multicolumn{7}{l}{$CD$: Load duration factor.}\\
\multicolumn{7}{l}{$M_z$: Bending moment.}\\
\multicolumn{7}{l}{$CL$: Beam stability factor.}\\
\multicolumn{7}{l}{$\nu$: Bending efficiency.}\\
\multicolumn{7}{l}{$V_y$: Shear force.}\\
\multicolumn{7}{l}{$\mu$: Shear efficiency.}\\
\hline  
  \end{tabular}
\end{small}
\caption{Roof rafters structural integrity calculation results}\label{tb_roof_rafters_structural_integrity}
\end{center}
\end{table}

The maximum bending efficiency is 0.68 and the maximum shear efficiency is 0.53, both for load case $LC2= D+S$. Both values are lower than one, so the strength of the rafters is adequate.\\

The maximum deflection is 13.70 mm (L/257; L= 3.52 m) which is also acceptable.\\

We have also checked an alternate design using TJI PRI-30 (9-1/2 in) performance rated I-joists and the results are equally satisfactory.\\ 

\begin{figure}
  \begin{center}
  \includegraphics[width=120mm]{figures/wood_structure/joists/rafters_dimensions_rev01}
  \end{center}
  \caption{Rafters key view.}\label{fg_rafters_dimensions_rev01}
\end{figure}

\subsubsection{Scissor trusses}
The design of the scissor trusses will be performed by the truss manufacturer.

\subsubsection{Roof joists and beams}\label{sc_roof_joists_and_beams}
The joists and beams that cover the space between gable roofs are shown in figure \ref{fg_roof_floor_joists_and_lvl_beams}.\\

\begin{figure}
  \begin{center}
  \includegraphics[width=120mm]{figures/wood_structure/joists/roof_floor_joists_and_lvl_beams}
  \end{center}
  \caption{Roof joists and beams.}\label{fg_roof_floor_joists_and_lvl_beams}
\end{figure}

\begin{figure}
  \begin{center}
    \begin{small}
\begin{tabular}{|c|c|}
      \hline
      % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
      \includegraphics[width=60mm]{figures/wood_structure/joists/roof_beams_and_joists_dead_load} & \includegraphics[width=60mm]{figures/wood_structure/joists/roof_beams_and_joists_live_load} \\
      {\small Dead load} & {\small Live load} \\
      \includegraphics[width=60mm]{figures/wood_structure/joists/roof_beams_and_joists_wind_load} & \includegraphics[width=60mm]{figures/wood_structure/joists/roof_beams_and_joists_snow_load} \\
      {\small Wind load} & {\small Snow load} \\
      \hline
\end{tabular}
    \end{small}
  \end{center}
  \caption{Loads on roof joists and beams.} \label{fg_roof_floor_joists_and_lvl_beams_loads}
  \end{figure}

The loads on each member are presented in figure \ref{fg_roof_floor_joists_and_lvl_beams_loads}. The snow load on this part of the roof has been calculated taking into account the load due to sliding snow (see section \ref{sc_snow_load}). The load combinations used for the analysis are listed in table \ref{SLS}.\\

The value of the maximum deflection (see table \ref{tb_roof_floor_joists_and_lvl_beams_deflections}) is $19.57\ mm\ (L/352)$ which is smaller than the deflection limit $(L/240)$. 

\begin{table}
  \begin{center}
    \begin{small}
\begin{tabular}{lrlrll}
\hline
              &   \multicolumn{2}{c}{deflection limit}   &   \multicolumn{2}{c}{maximum deflection}   & \\
              &   $(mm)$           &                      &    $(mm)$           &     & \\
\hline
SLSdflEQ1609 &             38.37 & L/180                  &           15.18 & L/455                & OK           \\
 SLSdflEQ1610 &             28.77 & L/240                  &           16.77 & L/411                & OK           \\
 SLSdflEQ1611 &             28.77 & L/240                  &           19.56 & L/353                & OK           \\
 SLSdflEQ1612 &             28.77 & L/240                  &            0.07 & L/98040              & OK           \\
 SLSdflEQ1613 &             28.77 & L/240                  &           19.57 & L/352                & OK           \\
\hline
\end{tabular}
    \end{small}
  \end{center}
  \caption{Roof joists and beams. Maximum deflections}\label{tb_roof_floor_joists_and_lvl_beams_deflections}
\end{table}

Figure \ref{fg_roof_beams_and_joists_bending_efficiency} shows the bending efficiency of these beams and joists. The maximum value is 0.9 and this means that the bending strength in the worst case is $1.11$ times greater than the bending moment. In regard to the shear strength, the efficiency values are displayed in figure \ref{fg_roof_beams_and_joists_shear_efficiency}. As in the previous case, the efficiency values are always smaller than 1.0, so they guarantee the adequacy of the wood members.

\begin{figure}
  \begin{center}
  \includegraphics[width=120mm]{figures/wood_structure/joists/roof_beams_and_joists_bending_efficiency}
  \end{center}
  \caption{Roof joists and beams. Bending efficiency}\label{fg_roof_beams_and_joists_bending_efficiency}
\end{figure}

\begin{figure}
  \begin{center}
  \includegraphics[width=120mm]{figures/wood_structure/joists/roof_beams_and_joists_shear_efficiency}
  \end{center}
  \caption{Roof joists and beams. Shear efficiency}\label{fg_roof_beams_and_joists_shear_efficiency}
\end{figure}

\subsubsection{Attic joists}
These joists are subjected to the tension introduced by the rafters and to the bending due to the dead and live loads. The forces introduced by the rafters are the following:\\


\begin{center}
\begin{small}
  \begin{tabular}{lrrr}
    \hline
    \multicolumn{4}{c}{Loads on attic joists}\\
\hline
Load case  &    N $(kN)$  &   $R_{v,left}\ (kN/m)$ &   $R_{v,right}\ (kN/m)$ \\
\hline
 D  & 2.68 &               8.78 &                8.78 \\
 L  & 1.29 &               4.22 &                4.22 \\
 W  & 0.63 &              -0.49 &               -0.49 \\
 S  & 1.58 &               5.2  &                5.2  \\
 \hline
\multicolumn{4}{l}{where:}\\
\multicolumn{4}{p{80mm}}{$N$: Axial load (tension) on the attic joists.}\\
\multicolumn{4}{p{80mm}}{$R_{v,left}$: Vertical reaction per unit length at left support.}\\
\multicolumn{4}{p{80mm}}{$R_{v,right}$: Vertical reaction per unit length at right support.}\\
\hline
\end{tabular}
\end{small}
\end{center}


The joists are 2x10 dimensional lumber (timber: southern pine structural grade). We have also checked an alternate design using TJI PRI-30 (11-7/8 in) performance rated I-joists and the results are equally satisfactory.\\

The load combinations used for strength design are:

\begin{center}
\begin{small}
  \begin{tabular}{|l|l|}
    \hline
    \multicolumn{2}{|c|}{Loads combinations attic joists}\\
    \hline
    LC1 & $D$ \\
    LC2 & $D+L$ \\
    LC3 & $D+S$ \\
    LC4 & $D+0.75 L+0.75 S$ \\
    LC5 & $D+0.6 W$ \\
    LC6 & $D+0.45 W+0.75 L+0.75 S$\\
    LC7 & $0.6 D+0.6 W$\\
    \hline
  \end{tabular}
  \end{small}
  \end{center}

The internal forces and stresses obtained for those load combinations are:

\begin{center}
\begin{small}
\begin{tabular}{lrrrrrrrrrrr}
\hline
\multicolumn{12}{c}{Attic joists structural integrity calculation results}\\
\hline
     &    $T$ &     $M_z$ &   CD &     $f_t$ &    $F_t^*$ &     $F_b^*$ &   CL &   $F_b^{**}$ &   $\nu$ &   $V$ & $\eta$ \\
     &    $(kN)$ &     $(kN \cdot m)$ &   &     $(MPa)$ &    $(MPa)$ & $(MPa)$ &   &   $(MPa)$ &   &   $(kN)$ & \\
\hline
LC1 & 2.68 & -0.48 & 0.9  & 0.53 &   1.7 & 11.73 &    1 &   11.73 &         0.4  & -0.38 &      0.06 \\
 LC2 & 3.96 & -2.74 & 1.25 & 0.78 &   1.7 & 11.73 &    1 &   11.73 &         0.95 & -2.2  &      0.35 \\
 LC3 & 4.26 & -0.48 & 1.15 & 0.84 &   1.7 & 11.73 &    1 &   11.73 &         0.58 & -0.38 &      0.06 \\
 LC4 & 4.83 & -2.18 & 1.15 & 0.96 &   1.7 & 11.73 &    1 &   11.73 &         0.95 & -1.75 &      0.28 \\
 LC5 & 3.05 & -0.48 & 1.6  & 0.6  &   1.7 & 11.73 &    1 &   11.73 &         0.44 & -0.38 &      0.06 \\
 LC6 & 5.11 & -2.18 & 1.6  & 1.01 &   1.7 & 11.73 &    1 &   11.73 &         0.98 & -1.75 &      0.28 \\
 LC7 & 1.98 & -0.29 & 1.6  & 0.39 &   1.7 & 11.73 &    1 &   11.73 &         0.28 & -0.23 &      0.04 \\
\hline
\multicolumn{12}{l}{where:}\\
\multicolumn{12}{l}{$T$: Axial load (tension when greater than 0).}\\
\multicolumn{12}{l}{$M_z$: Bending moment.}\\
\multicolumn{12}{l}{$CD$: Load duration factor.}\\
\multicolumn{12}{l}{$f_t$: Tensile stress in the joist.  }\\
\multicolumn{12}{l}{$F_t^*$: Adjusted value of the admissible tensile stress of the joist wood.  }\\
\multicolumn{12}{l}{$F_b$: Admissible tensile stress of the joist wood.  }\\
\multicolumn{12}{l}{$CL$: Beam stability factor.}\\
\multicolumn{12}{l}{$F_b$: Adjusted value of the admissible tensile stress of the joist wood.  }\\
\multicolumn{12}{l}{$\nu$: Bending efficiency.}\\
\multicolumn{12}{l}{$V_y$: Shear force.}\\
\multicolumn{12}{l}{$\mu$: Shear efficiency.}\\
\hline
\end{tabular}
  \end{small}
\end{center}


The maximum bending efficiency is 0.98 and the maximum shear efficiency is 0.28, both for load case $LC6= D+0.45 W+0.75 L+0.75 S$. Both results are lower than one, so the strength of the joists is sufficient.\\

The maximum deflection is 9.13 mm (L/546; L= 5.00 m) which is also acceptable. 

\begin{figure}
  \begin{center}
  \includegraphics[width=120mm]{figures/wood_structure/joists/bottom_chords_dimensions_rev01}
  \end{center}
  \caption{Attic joists key view.}\label{fg_bottom_chords_dimensions_rev01}
\end{figure}

\subsubsection{Second floor corridors beams and joists} \label{sc_2nd_floor_joists_and_beams}
The figures \ref{fg_second_floor_beams_and_joists_rev01} and \ref{fg_north_facade_cantilever} give the definition of the beam and joists members that support the corridors and the north facade balcony on the second floor.\\

\begin{figure}
  \begin{center}
  \includegraphics[width=120mm]{figures/wood_structure/joists/second_floor_beams_and_joists_rev01}
  \end{center}
  \caption{2\textsuperscript{nd} floor corridors beams and joists.}\label{fg_second_floor_beams_and_joists_rev01}
\end{figure}

%%\begin{wrapfigure}{r}{0.45\textwidth}
\begin{figure}{r}
  \begin{center}
  \includegraphics[width=90mm]{figures/wood_structure/joists/north_facade_cantilever}
  \end{center}
  \caption{2\textsuperscript{nd} floor north facade balcony beams and joists.}\label{fg_north_facade_cantilever}
%%\end{wrapfigure}
\end{figure}

The loads acting on those members are represented in figure \ref{fg_2nd_floor_floor_joists_and_lvl_beams_loads}.\\

\begin{figure}
  \begin{center}
    \begin{small}
\begin{tabular}{|c|c|}
      \hline
      % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
      \includegraphics[width=60mm]{figures/wood_structure/joists/2nd_floor_beams_and_joists_dead_load} & \includegraphics[width=60mm]{figures/wood_structure/joists/2nd_floor_beams_and_joists_live_load} \\
      {\small Dead load} & {\small Live load} \\
      \includegraphics[width=60mm]{figures/wood_structure/joists/2nd_floor_beams_and_joists_wind_load} & \includegraphics[width=60mm]{figures/wood_structure/joists/2nd_floor_beams_and_joists_snow_load} \\
      {\small Wind load} & {\small Snow load} \\
      \hline
\end{tabular}
    \end{small}
  \end{center}
  \caption{Loads on roof joists and beams.} \label{fg_2nd_floor_floor_joists_and_lvl_beams_loads}
  \end{figure}

The value of the maximum deflection (see table \ref{tb_2nd_floor_floor_joists_and_lvl_beams_deflections}) is 14.34 mm (L/481) which is smaller than the deflection limit (L/240).\\

\begin{table}
  \begin{center}
    \begin{small}
\begin{tabular}{lrlrll}
\hline
              &   \multicolumn{2}{c}{deflection limit}   &   \multicolumn{2}{c}{maximum deflection}   & \\
              &   $(mm)$           &                      &    $(mm)$           &     & \\
\hline
 SLSdflEQ1609 &             38.37 & L/180                  &           25.9  & L/266                & OK           \\
 SLSdflEQ1610 &             28.77 & L/240                  &            2.26 & L/3057               & OK           \\
 SLSdflEQ1611 &             28.77 & L/240                  &           14.34 & L/481                & OK           \\
 SLSdflEQ1612 &             28.77 & L/240                  &            0.13 & L/54505              & OK           \\
 SLSdflEQ1613 &             28.77 & L/240                  &           14.34 & L/481                & OK           \\
\hline
\end{tabular}
    \end{small}
  \end{center}
  \caption{Second floor joists and beams. Maximum deflections}\label{tb_2nd_floor_floor_joists_and_lvl_beams_deflections}
\end{table}

Figure \ref{fg_2nd_floor_beams_and_joists_bending_efficiency} shows the bending efficiency of these beams and joists. The maximum value is 0.9 and this means that the bending strength in the worst case is $1.11$ times greater than the bending moment. In regard to the shear strength, the efficiency values are displayed in figure \ref{fg_2nd_floor_beams_and_joists_shear_efficiency}. The efficiency values are again smaller than 1.0, so they guarantee the adequacy of the wood members.\\

\begin{figure}
  \begin{center}
  \includegraphics[width=120mm]{figures/wood_structure/joists/2nd_floor_beams_and_joists_bending_efficiency}
  \end{center}
  \caption{Roof joists and beams. Bending efficiency}\label{fg_2nd_floor_beams_and_joists_bending_efficiency}
\end{figure}

\begin{figure}
  \begin{center}
  \includegraphics[width=120mm]{figures/wood_structure/joists/2nd_floor_beams_and_joists_shear_efficiency}
  \end{center}
  \caption{Roof joists and beams. Shear efficiency}\label{fg_2nd_floor_beams_and_joists_shear_efficiency}
\end{figure}
