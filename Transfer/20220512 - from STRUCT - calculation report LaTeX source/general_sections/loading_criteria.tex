%\section{Loading criteria}
The following is a summary of the loading criteria used during design: 

\subsection{Dead load (DL)}
The superimposed dead loads,  in addition to the self weight of the structure, are calculated according to the weight of the materials that make up the cladding, coverings, finishes, \ldots, taking into account the minimum design dead loads  from table C3.1-1 of ASCE7-16. \\

The following tables show the dead loads obtained for each component, expressed in psf. Figures \ref{roof_load_distribution} to \ref{2floor_load_distribution} display the load distribution over the building.

\input{roof_dead_loads.tex}
\input{ceiling_dead_loads.tex}
\input{floor_dead_loads.tex}

\begin{figure}
  \includegraphics[width=\linewidth]{figures/roof_load_distribution.png}
  \caption{Loads distribution over roof}\label{roof_load_distribution}
\end{figure}
\begin{figure}
  \includegraphics[width=\linewidth]{figures/attic_load_distribution.png}
  \caption{Loads distribution over attic}\label{attic_load_distribution}
\end{figure}
\begin{figure}
  \includegraphics[width=\linewidth]{figures/2floor_load_distribution.png}
  \caption{Loads distribution over 2nd floor}\label{2floor_load_distribution}
\end{figure}



\subsection{Live loads (LL)}
\subsubsection{Main building}
%The gravity loads listed in Table \ref{grav_load} are in addition to the self weight of the structure. The minimum loading requirements were taken from UDC chapter SPS 321. Loads are given in pounds per square foot (psf).
Live loads in the main building, accordingly to table 321.02-1 of UDC code chapter 321, are listed in the following table:

  \begin{center}
   \begin{tabular}{lr}
      \textbf{Use} & \textbf{Live Loading} \\
      \hlineB{2}
      \arrayrulecolor{gray}\hline
      Floors & 40 psf $(1.92\ kN/m^2)$\\
      \arrayrulecolor{gray}\hline
      Garage floors & 50 psf  $(2.39\ kN/m^2)$ \\
      \arrayrulecolor{gray}\hline
      Exterior balconies, decks, porches & 40 psf $(1.92\ kN/m^2)$ \\
      \arrayrulecolor{gray}\hline
      Ceilings (with storage) & 20 psf $(0.96\ kN/m^2)$ \\
      \arrayrulecolor{gray}\hline
      Ceilings (without storage) & 5 psf $(0.24\ kN/m^2)$\\
      \hlineB{2}
  \end{tabular}
  \end{center}



\subsubsection{Tornado shelter}
The live load on the roof of tornado shelters, according to 2020 ICC 500 standard, shall be not less than \textbf{100 psf} $(4.8\ kN/m^2)$. In addition, the roof is supposed subject to the falling of debris coming from the eventual collapse of the main building. This last load is estimated as \textbf{90 psf} $(4.3\ kN/m^2)$. 

\subsection{Snow load (SL)}\label{sc_snow_load}
Snow loads on flat and sloped roofs, as well as the loads caused by the snow sliding off the sloped roofs onto the lower roof, are calculated below, accordingly to the code ASCE 7-16:
\input{general_sections/snow_load_ASCE_7_10.tex}

The snow loads calculated as stated by the code ASCE 7-16, even in case of snow drift (18.48 + 11.34 = 29.82 psf), have smaller values than the snow load proposed by the code \emph{UDC chapter SPS 320}. The zone map in this code (fig. \ref{snow_zone_map}) suggests for Zone 2 a snow load of \textbf{30 psf} $(0.24\ kN/m^2)$, that, consequently, could be adopted for all surfaces, on the conservative side.

\begin{center}
  \begin{figure}[h]
    \centering
  \includegraphics[width=70mm]{figures/roof_loads.png}
  \caption{Zone map for roof snow loads \emph{(UDC chapter SPS 320)}}\label{snow_zone_map}
\end{figure}
\end{center}
%Figures \ref{roof_load_distribution} to \ref{2floor_load_distribution} display the live load distribution over the building.

\subsection{Wind design criteria}
\subsubsection{Main building}
On the conservative side, the wind load provisions of ASCE 7-22 are adopted for the calculations, since the  wind pressure values obtained are slightly higher than those derived from ASCE 7-16 criteria.

Wind loading, calculated in accordance with Figs. 26.5-1B and CC.2-1–CC.2-4, and Section 26.5.2 of ASCE/SEI 7-22, is shown in Table \ref{wind_load}.

\begin{table}[h]
  \begin{center}
  \caption{\textbf{Wind Design Criteria}} \label{wind_load}
    \begin{tabular}{ll}
      \textbf{Parameter} & \textbf{Value} \\
      \hlineB{2}
Basic wind speed (*) &  47.8 m/s (109 Vmph)\\
      \arrayrulecolor{gray}\hline
Exposure & C \\
      \arrayrulecolor{gray}\hline
Risk Category & II \\
      \arrayrulecolor{gray}\hline
Importance Factor ($I_w$ ) & 1.0 \\
\arrayrulecolor{gray}\hline
Ground elevation above sea level ($z_e$) & 258 m (848 ft) \\
      \arrayrulecolor{gray}\hline
    Topographic Factor ($K_{zt}$ ) & 1.0\\
      \arrayrulecolor{gray}\hline
     Enclosure Classification & partially enclosed \\
     \hlineB{2}
     \multicolumn{2}{p{10cm}}{(*) \small{3-second gust wind speed at 33 ft above ground for Exposure C Category, corresponding to approximately a 7\% probability of exceedance in 50 years}}\\
  \end{tabular}
  \end{center}
\end{table}


Site is not in a hurricane-prone region as defined in ASCE/SEI 7-22 Section 26.2.\\


\paragraph{Directionality factor} is determined according to table 26.6-1 of ASCE7-22, that for buildings takes the value $K_d$=0.85.

\paragraph{Ground elevation factor} is determined, in accordance with art. 26.9 of ASCE7-22, from the following formula:
$$K_e = e^{-0.000119z_e} = 0.97 $$

\paragraph{Gust-effect factor} as per art. 26.11.1  of ASCE7-22 can be taken for a rigid building or other structure as G=0.85.

\paragraph{Velocity pressure exposure coefficient $K_z$} is determined from the following formula:
\begin{equation} \label{eq_K_z}
  K_z(z)=
  \begin{cases}
    2.41 \cdot (\frac{z}{Z_g})^{2/\alpha}, & \text{if } 4.6\ m \le z \le Z_g \\
    2.41 \cdot (\frac{4.6}{Z_g})^{2/\alpha} & \text{if } z < 4.6\ m
  \end{cases}
\end{equation}

\noindent from table 26.11-1 of ASCE7-22, exposure C and SI units:

\begin{align}
  \alpha &= 9.8\\
  Z_g &= 275.0
  \end{align}

\paragraph{Velocity pressure} $q_z$ (N/m$^2$) is evaluated by the following equation:
$$q_z = 0.613 \cdot K_z \cdot K_{zt} \cdot K_e \cdot V^2 $$
where the basic wind speed \emph{v} is expressed in m/s.

\begin{figure}
  \centering
  \includegraphics[width=120mm]{figures/main_wind_force_resisting_system.png}
  \caption{Main wind force resisting system \emph{(fig. 27.3-1 ASCE7-22)}}\label{wind_force_resisting_system}
\end{figure}

\paragraph{Internal pressure coefficient $GC_{pi}$} is taken from table 26.13-1 of ASCE 7, for partially enclosed buildings $GC_{pi}=\pm 0.55$.\\

Figures \ref{windEWext_load_distribution} to \ref{intNeg_load_distribution} show the wind loads distribution over a transverse section of the building. Some parameter values and wind-coefficients calculated are listed below:
\begin{center}
  \begin{tabular}{l}
Eave height=  7.36  m \\
Mean roof height=  8.74  m \\
$K_z$ eave= 0.94 \\
$K_z$ mean roof height= 0.97 \\
Velocity pressure $q_z$ at eave height=  1.08  $kN/m^2$ \\
Velocity pressure $q_z$ mean at roof height=  1.12  $kN/m^2$ \\
Horizontal dimension of building, measured parallel to wind direction L= 12.82  m \\
Horizontal dimension of building, measured normal to wind direction B= 12.37  m \\
$C_p$ windward wall= 0.8 \\
$C_p$ leeward wall= -0.49 \\
$C_p$ sideward wall= -0.7 \\
Wind design external pressure on leeward wall = -0.45  $kN/m^2$ \\
 Angle of plane of roof from horizontal=  32.3  degrees \\
$q_z$ at mean roof = 1.12  $kN/m^2$ \\
$C_p$ windward roof=  0.23 \\
Wind design external pressure on windward roof =  0.22  $kN/m^2$ \\
$C_p$ leeward roof=  -0.6 \\
Wind design external pressure on leeward roof =  -0.57  $kN/m^2$ \\
  \end{tabular}
  \end{center}
    
 \begin{figure}
    \centering
  \includegraphics[width=100mm]{calc_results/wind_react/wind_loads/windEWext.png}
  \caption{\footnotesize{External pressures over building transverse-section for wind in East-West direction}}\label{windEWext_load_distribution}
\end{figure}
 \begin{figure}[h]
    \centering
  \includegraphics[width=100mm]{calc_results/wind_react/wind_loads/windWEext.png}
  \caption{\footnotesize{External pressures over building transverse-section for wind in West-East direction}}\label{windWEext_load_distribution}
\end{figure}
\begin{figure}[h]
    \centering
  \includegraphics[width=100mm]{calc_results/wind_react/wind_loads/intPos.png}
  \caption{\footnotesize{Internal positive wind pressures over building transverse-section}}\label{intPos_load_distribution}
\end{figure}
\begin{figure}[h]
    \centering
  \includegraphics[width=100mm]{calc_results/wind_react/wind_loads/intNeg.png}
  \caption{\footnotesize{Internal negative wind pressures over building transverse-section}}\label{intNeg_load_distribution}
\end{figure}
%\afterpage{\clearpage}
\clearpage

\subsection{Tornado shelter}
According to \emph{2020 ICC 500 Standard for the Design and Construction of Storm Shelters}, the design wind speed that corresponds to the site is \textbf{$v_T$= 250 mph = 111,7 m/s} (see fig. \ref{wind_speed_tornado}).\\

The tables below summarize the wind parameters and pressure coefficients obtained for the design of the structure. Figures \ref{WLSNawalls} to \ref{WLintNegoverallSet} in section \ref{shelter_results}, show the wind load distribution over the walls and roof of the shelter.
\input{shelter/wind_shelter.tex}

\begin{figure}
    \centering
  \includegraphics[width=100mm]{figures/tornado/wind_speed.png}
  \caption{\footnotesize{Design wind speed $v_T$ for tornadoes \emph{fig. 304.2 500 ICC/NSSA Standard}}}\label{wind_speed_tornado}. 
\end{figure}


